# -*- coding: utf-8 -*-

import pandas as pd



df = pd.read_csv('dataset.csv', sep=';')

df.head()

df.info()

#1
#Calcule el valor promedio por metro cuadrado de un municipio dado, es decir, el nombre del municipio es un parámetro de la función.
#valor_promedio(“BOGOTÁ”), retornaría 6.05
def valor_promedio(municipio):
  try:
    df["area_precio"] = df["precio"] / df["area"]
    return round(df["area_precio"].where((df["municipio"].str.lower())==municipio.lower()).dropna().mean(),2)
  except:
    print("No se encontró municipio")
  
valor_promedio("Bogota")

#2
#Retorne una lista ordenada de precios para un municipio y tipo dado, es decir, el municipio y el tipo son parámetros de la función.
#lista_precios(“MEDELLÍN”, “APARTAMENTO”), retornaría [670,300]
def lista_precios(municipio, tipo):
  try:
    df_filtro=df.where((df["municipio"].str.lower())==municipio.lower()).dropna()
    df_filtro=df_filtro.where((df["tipo"].str.lower())==tipo.lower()).dropna()  
    return df_filtro["precio"].astype(int).sort_values(ascending=False).tolist()
  except:
    print("No se encontró municipio o no se encontró tipo")
  

lista_precios("MEDELLIN","APARTAMENTO")

#3
#Retorne una lista ordenada de los precios que sean iguales o mayores a un valor dado para un municipio y tipo específico. Tanto el valor, como el nombre de municipio y tipo son parámetros de la función.
#lista_precios_umbral(“MEDELLÍN”, “APARTAMENTO”, 200), retornaría [300]
def lista_precios_umbral(municipio, tipo, umbral):
  
  try:
    df_filtro=df.where((df["municipio"].str.lower())==municipio.lower()).dropna()
    df_filtro=df_filtro.where((df["tipo"].str.lower())==tipo.lower()).dropna() 
    df_filtro=df_filtro.where(df["precio"]>=umbral).dropna()
    return df_filtro["precio"].astype(int).sort_values(ascending=False).tolist()
  except:
    print("No se encontró municipio o no se encontró tipo")


lista_precios_umbral("medellin","apartamento",200)

#4
#Retorne el promedio del área de todos los predios de la tabla.
#promedio_area(), retornaría 124.3
def promedio_area():
  return df["area"].mean()

promedio_area()

#5
#Retorne una lista ordenada de los municipios que tienen predios registrados en la tabla. Los nombres no pueden ir repetidos.
#lista_municipios(), retornaría [“BOGOTÁ”, “CALI”, “CHÍA”, “MEDELLÍN”, SOPÓ”]
def lista_municipios():
  return df["municipio"].unique().tolist()

lista_municipios()

#6
#Retorne una tupla con las áreas de los predios de un tipo dado. El tipo es un parámetro de la función.
#lista_areas(“CASA”), retornaría (100, 220, 240, 260)
def lista_areas(tipo):
  try:
    filtro = df.where((df["tipo"].str.lower())==tipo.lower()).dropna()
    lista = filtro["area"].astype(int).sort_values().tolist()
    return tuple(lista)
  except:
    print("Tipo no encontrado")

lista_areas("CASA")

#7
#Retorne un diccionario con los datos completos de un predio dado. El código del predio es un parámetro de la función.
#ver_predio(5) retornaría { “Código_predio”:5 , “Área”:32, “Municipio”: “Cali”, “Precio”:400, “Tipo”:”Apartamento”}

def ver_predio(codigo_predio):
  try:
    filtro = df.where(df["codigo_predio"]==codigo_predio).dropna()
    diccionario = {
        "codigo_predio": filtro.iloc[:,0].values[0],
        "area": filtro.iloc[:,1].values[0],
        "municipio":filtro.iloc[:,2].values[0],
        "precio":filtro.iloc[:,3].values[0],
        "tipo":filtro.iloc[:,4].values[0],
    }  
    return diccionario
  except:
    print("Predio no encontrado")


ver_predio(5)